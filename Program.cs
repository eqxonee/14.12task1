﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14._12_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int n;
            int[] mas;
            Console.Write("Input qollvo: ");
            n = int.Parse(Console.ReadLine());

            mas = new int[n];

            for (int i = 0; i < n; i++)
            {
                mas[i] = rnd.Next(-100, 100);
            }
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{mas[i]} ");
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                if (mas[i] < 0)
                {
                    Console.Write($"{mas[i]} ");
                }
            }
            for (int i = 0; i < n; i++)
            {
                if (mas[i] >= 0)
                {
                    Console.Write($"{mas[i]} ");
                }
            }

            Console.ReadKey();
        }
    }
}
